#include "umd.h"
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include "util.h"

int main(int argc, char **argv)
{
	int x, y, button;

	int status = VideoInit();

	if (status != 0)
		return status;

	if (UMD_Init() < 0) {
		fprintf(stderr, "%s\n", UMD_GetError());
		return 1;
	}

	if (UMD_Warp(100, 100) < 0) {
		fprintf(stderr, "%s\n", UMD_GetError());
		return 3;
	}

	TestWait(1);

	if (UMD_Move(-25, 50) < 0) {
		fprintf(stderr, "%s\n", UMD_GetError());
		return 4;
	}

	CollectInput(&x, &y, &button);

	if (UMD_Quit() < 0) {
		fprintf(stderr, "%s\n", UMD_GetError());
		return 2;
	}

	VideoQuit();

	if (x != 75 || y != 150) {
		fprintf(stderr, "Got\t%d\t%d\n", x, y);
		fprintf(stderr, "Expected\t75\t150\n"); 
		fprintf(stderr, "Test failed\n");
		return 5;
	}

	printf("Test successful!\n");

	return 0;
}
