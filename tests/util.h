#include <SDL/SDL.h>

#if defined(_WIN32) || defined(__WIN32__)
#include <windows.h>

inline void TestWait(int seconds)
{
	Sleep(1000*seconds);
}

#else
#include <unistd.h>

inline void TestWait(int seconds)
{
	sleep(seconds);
}

#endif


inline int VideoInit()
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "Could not initiliaze video: %s\n", SDL_GetError());
		return 10;
	}

	const SDL_VideoInfo *vi = SDL_GetVideoInfo();
	if (!vi) {
		fprintf(stderr, "Could not get video info: %s", SDL_GetError());
		return 11;
	}

	if (!SDL_SetVideoMode(vi->current_w, vi->current_h, 0, SDL_FULLSCREEN)) {
		fprintf(stderr, "Could not set a fullscreen video mode: %s", SDL_GetError());
		return 12;
	}
	
	TestWait(2);

	return 0;
}

inline int CollectInput(int *x, int *y, int *button)
{
	TestWait(1);

	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		switch(e.type) {
			case SDL_MOUSEMOTION:
				*x = e.motion.x;
				*y = e.motion.y;
				break;
			case SDL_MOUSEBUTTONDOWN:
				*button = 1;
				break;
		}
	}

	return 0;
}

inline int VideoQuit()
{
	SDL_Quit();
}

