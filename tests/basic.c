#include "umd.h"
#include <stdio.h>
#include <unistd.h>

int main(int argc, char **argv)
{
	if (UMD_Init() < 0) {
		fprintf(stderr, "%s\n", UMD_GetError());
		return 1;
	}

	if (UMD_Quit() < 0) {
		fprintf(stderr, "%s\n", UMD_GetError());
		return 2;
	}

	printf("Test successful!\n");

	return 0;
}
