/*************************************************************************
	Copyright (C) 2009 Matthew Thompson <chameleonator@gmail.com>

    This file is part of libumd.

    libumd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    libumd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with libumd.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/

#include "umd.h"
#include "umd_private.h"

#if defined(_WIN32) || defined(__WIN32__)

#define _WIN32_WINNT 0x0500
#include <windows.h>

int UMD_Init()
{
	return 0;
}

int UMD_Quit()
{
	return 0;
}

int UMD_Warp(int x, int y)
{
	SetCursorPos(x, y);

	return 0;
}

int UMD_Move(int x, int y)
{
	POINT point;

	if (GetCursorPos(&point) == 0) {
		SET_ERROR("Could not get current cursor position to move");
		return -1;
	}

	SetCursorPos(point.x + x, point.y + y);

	return 0;
}

static int get_flags(int button, int state)
{
	int flags = 0;

	switch(button) {
		case UMD_LEFT:
			flags = state? MOUSEEVENTF_LEFTDOWN : MOUSEEVENTF_LEFTUP;
			break;
		case UMD_RIGHT:
			flags = state? MOUSEEVENTF_RIGHTDOWN : MOUSEEVENTF_RIGHTUP;
			break;
		case UMD_MIDDLE:
			flags = state? MOUSEEVENTF_MIDDLEDOWN : MOUSEEVENTF_MIDDLEUP;
			break;
		default:
			SET_ERROR("Unrecognized mouse button in click");
			return 0;
	}

	return flags;
}

int UMD_Click(int button, int state)
{
	INPUT input;
	int flags = get_flags(button, state);

	if (flags == 0)
		return -1;

	memset(&input, 0, sizeof input);
	input.type = INPUT_MOUSE;
	input.mi.dwFlags = flags;

	if (SendInput(1, &input, sizeof input) != 1) {
			SET_ERROR("Could not send click event");
			return -1;
	}

	return 0;
}

int UMD_ClickAt(int button, int state, int x, int y)
{
	INPUT input;
	int flags = get_flags(button, state);

	if (flags == 0)
		return -1;

	memset(&input, 0, sizeof input);
	input.type = INPUT_MOUSE;
	input.mi.dwFlags = flags | MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE;
	input.mi.dx = x;
	input.mi.dy = y;

	if (SendInput(1, &input, sizeof input) != 1) {
			SET_ERROR("Could not send click at event");
			return -1;
	}

	return 0;
}

int UMD_SingleClick(int button)
{	
	if (UMD_Click(button, 1) < 0)
		return -1;

	if (UMD_Click(button, 0) < 0)
		return -1;

	return 0;
}

int UMD_SingleClickAt(int button, int x, int y)
{
	if (UMD_ClickAt(button, 1, x, y) < 0)
		return -1;
	
	if (UMD_Click(button, 0) < 0)
		return -1;

	return 0;
}

#endif




