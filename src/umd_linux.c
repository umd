/*************************************************************************
	Copyright (C) 2009 Matthew Thompson <chameleonator@gmail.com>

    This file is part of libumd.

    libumd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    libumd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with libumd.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/

#include "umd.h"
#include "umd_private.h"

#if defined(__linux) || defined(linux)

#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/input.h>
#include <linux/uinput.h>
#include <unistd.h>

static int fd = -1;
static struct uinput_user_dev uidev;
static struct input_event event;

static char *dev_filename[] = {"/dev/uinput", "/dev/input/uinput", "/dev/misc/uinput", 0};

int UMD_Init()
{
	int i;
	unsigned int events[] = { EV_SYN, EV_KEY, EV_ABS, EV_REL, EV_MAX };
	unsigned int keys[] = { BTN_MOUSE, BTN_LEFT, BTN_RIGHT, BTN_MIDDLE, KEY_MAX };
	unsigned int mouseabs[] = { ABS_X, ABS_Y, ABS_MAX };
	unsigned int mouserel[] = { REL_X, REL_Y, REL_MAX };

	for (i=0; dev_filename[i] != 0; i++) {
		if ((fd = open(dev_filename[i], O_WRONLY)) >= 0)
			break;
	}

	if (fd <= 0) {
		switch (errno) {
			case EACCES: case EROFS:
				SET_ERROR("Could not open uinput device due to permissions");
				break;
			case ENODEV: case ENXIO: case ENOTDIR:
				SET_ERROR("Could not find uinput device to open");
				break;
			default:
				SET_ERROR("Could not open uinput device");
		}
		return -1;
	}

	memset(&event, 0, sizeof event);

	memset(&uidev, 0, sizeof uidev);
	snprintf(uidev.name, UINPUT_MAX_NAME_SIZE, "UMD Virtual Mouse (userspace driver)");

	if (write(fd, &uidev, sizeof uidev) != sizeof uidev) {
		SET_ERROR("Could not write to uinput device");
	}

	for (i=0; events[i] != EV_MAX; i++) {
		if (ioctl(fd, UI_SET_EVBIT, events[i]) < 0) {
			SET_ERROR("Could not set up uinput device");
			close(fd);
		}
	}

	for (i=0; keys[i] != KEY_MAX; i++) {
		if (ioctl(fd, UI_SET_KEYBIT, keys[i]) < 0) {
			SET_ERROR("Could not set up uinput device");
			close(fd);
		}
	}

	for (i=0; mouseabs[i] != ABS_MAX; i++) {
		if (ioctl(fd, UI_SET_ABSBIT, mouseabs[i]) < 0) {
			SET_ERROR("Could not set up uinput device");
			close(fd);
		}
	}

	for (i=0; mouserel[i] != REL_MAX; i++) {
		if (ioctl(fd, UI_SET_RELBIT, mouserel[i]) < 0) {
			SET_ERROR("Could not set up uinput device");
			close(fd);
		}
	}

	if (ioctl(fd, UI_DEV_CREATE, 0) < 0) {
		SET_ERROR("Could not create uinput device");
		close(fd);
		return -1;
	}

	usleep(500 * 1000); // TODO: replace this with something more sensible

	return 0;
}

int UMD_Quit()
{
	if (fd <= 0) {
		SET_ERROR("Could not quit, umd was not initialized");
		return -1;
	}

	if (ioctl(fd, UI_DEV_DESTROY) != 0) {
		SET_ERROR("Could not destroy virtual mouse device");
		close(fd);
		return -1;
	}

	close(fd);

	return 0;
}

static int motion_event(unsigned int type, int x, int y)
{
	unsigned int code_x, code_y;
	if (type == EV_ABS) {
		code_x = ABS_X;
		code_y = ABS_Y;
	}
	else {
		code_x = REL_X;
		code_y = REL_Y;
	}

	gettimeofday(&event.time, NULL);

	event.type = type;
	event.code = code_x;
	event.value = x;
	if (write(fd, &event, sizeof event) != sizeof event)
		return -1;

	event.type = type;
	event.code = code_y;
	event.value = y;
	if (write(fd, &event, sizeof event) != sizeof event)
		return -1;

	event.type = EV_SYN;
	event.code = SYN_REPORT;
	event.value = 0;
	if (write(fd, &event, sizeof event) != sizeof event)
		return -1;

	return 0;
}

int UMD_Warp(int x, int y)
{
	if (fd < 0) {
		SET_ERROR("Could not warp mouse, umd was not initialized");
		return -1;
	}

	if (motion_event(EV_ABS, x, y) < 0) {
		SET_ERROR("Could not warp mouse");
		return -1;
	}

	return 0;
}

int UMD_Move(int x, int y)
{
	if (fd < 0) {
		SET_ERROR("Could not move mouse, umd was not initialized");
		return -1;
	}

	if (motion_event(EV_REL, x, y) < 0) {
		SET_ERROR("Could not move mouse");
		return -1;
	}

	return 0;
}

int UMD_Click(int button, int state)
{
	unsigned int code;

	switch(button) {
		case UMD_LEFT:
			code = BTN_LEFT;
			break;
		case UMD_RIGHT:
			code = BTN_RIGHT;
			break;
		case UMD_MIDDLE:
			code = BTN_MIDDLE;
			break;
		default:
			SET_ERROR("Unrecognized mouse button in click");
			return -1;
	}

	gettimeofday(&event.time, NULL);

	event.type = EV_KEY;
	event.code = code;
	event.value = 1;
	if (write(fd, &event, sizeof event) != sizeof event)
		return -1;
	
	event.type = EV_SYN;
	event.code = SYN_REPORT;
	event.value = state?1:0;
	if (write(fd, &event, sizeof event) != sizeof event)
		return -1;

	return 0;

write_error:
	SET_ERROR("Could not set position of virtual device");
	close(fd);
	return -1;
}

int UMD_ClickAt(int button, int state, int x, int y)
{
	if (UMD_Warp(x, y) < 0)
		return -1;
	
	if (UMD_Click(button, state) < 0)
		return -1;

	return 0;
}

int UMD_SingleClick(int button)
{	
	if (UMD_Click(button, 1) < 0)
		return -1;

	if (UMD_Click(button, 0) < 0)
		return -1;

	return 0;
}

int UMD_SingleClickAt(int button, int x, int y)
{
	if (UMD_ClickAt(button, 1, x, y) < 0)
		return -1;
	
	if (UMD_Click(button, 0) < 0)
		return -1;

	return 0;
}

#endif




