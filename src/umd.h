/*************************************************************************
	Copyright (C) 2009 Matthew Thompson <chameleonator@gmail.com>

    This file is part of libumd.

    libumd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    libumd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with libumd.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/

#ifndef LIBUMD_H__
#define LIBUMD_H__

#ifdef __cplusplus
extern "C" {
#endif

#define UMD_LEFT   0x01
#define UMD_RIGHT  0x02
#define UMD_MIDDLE 0x04

int UMD_Init();

int UMD_Quit();

const char *UMD_GetError();


int UMD_Warp(int x, int y);

int UMD_Move(int x, int y);


int UMD_Click(int button, int state);

int UMD_ClickAt(int button, int state, int x, int y);


int UMD_SingleClick(int button);

int UMD_SingleClickAt(int button, int x, int y);

#ifdef __cplusplus
}
#endif

#endif

